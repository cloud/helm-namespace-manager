Example

First install flux
```
$ kubectl create ns helm-namespace-manager

$ flux install
```

Then create a git source for this repo.
```
$ flux create source git helm-namespace-manager \
    --namespace=helm-namespace-manager
    --url=https://gitlab.cern.ch/cloud/helm-namespace-manager \
    --branch=master \
    --interval=1m

$ kubectl -n helm-namespace-manager get gitrepo
NAME                     URL                                                   READY   STATUS                                                              AGE
helm-namespace-manager   https://gitlab.cern.ch/cloud/helm-namespace-manager   True    Fetched revision: master/b3baa9472b972ea44fc478490a4b5dea86654785   24s
```

Then create the example release with the kustomization.
```
$ flux create kustomization helm-namespace-manager \
    --namespace=helm-namespace-manager \
    --source=GitRepository/helm-namespace-manager \
    --path="./releases/example"

$ kubectl -n helm-namespace-manager get kustomization
NAME                     READY   STATUS                                                              AGE
helm-namespace-manager   True    Applied revision: master/b3baa9472b972ea44fc478490a4b5dea86654785   17s
```

That kustomization creates the helm release and values configmaps.

For the dynamic values, create this cronjob.
```
$ kubectl -n helm-namespace-manager create role --resource=configmap --verb=get,create,update configmap-role
$ kubectl -n helm-namespace-manager create rolebinding configmap-rolebinding --serviceaccount=helm-namespace-manager:default --role=configmap-role

$ kubectl -n helm-namespace-manager create cronjob data-job \
    --image=gitlab-registry.cern.ch/cloud/helm-namespace-manager/ldap-data-source:v0.1.2 \
    --schedule="*/1 * * * *" -- \
    /ldap-data-source \
    --input-configmap=namespace-values \
    --output-configmap=data-values \
    --ldap-properties=mail,uidNumber,gidNumber \
    --namespace=helm-namespace-manager
```

After the cron job has run, the data should be available in configmap `data-values`.
Maybe change the schedule to be less frequent after this.

Then the helm release should be able to reconcile properly.

```
$ kubectl -n helm-namespace-manager get gitrepo,kustomization,helmrelease
NAME                                                            URL                                                   READY   STATUS                                                              AGE
gitrepository.source.toolkit.fluxcd.io/helm-namespace-manager   https://gitlab.cern.ch/cloud/helm-namespace-manager   True    Fetched revision: master/bd342bc55535f3d4239a2c232c9a664790c8c180   27m

NAME                                                               READY   STATUS                                                              AGE
kustomization.kustomize.toolkit.fluxcd.io/helm-namespace-manager   True    Applied revision: master/bd342bc55535f3d4239a2c232c9a664790c8c180   24m

NAME                                            READY   STATUS                             AGE
helmrelease.helm.toolkit.fluxcd.io/my-release   True    Release reconciliation succeeded   24m
```

And the namespaces will appear.

```
$ cat custom.txt 
NAME          MAIL
metadata.name .metadata.annotations.xldap\.cern\.ch/mail

$ kubectl get ns it-dep-cm-rps-thartlan -o custom-columns-file=custom.txt
NAME                     MAIL
it-dep-cm-rps-thartlan   thomas.george.hartland@cern.ch
```
