{{- $uniqueDict := dict }}
{{- range $i, $group := .Values.personalNamespaces }}
  {{- range $j, $username := (index $.Values.data.groups $group.group) }}
    {{- $user := (index $.Values.data.users $username) }} 
    {{- $namespace := trimAll "-" (printf "%s-%s" (default $group.group $group.namePrefix) $username) }}
    {{- if hasKey $uniqueDict $namespace }}
      {{- fail (printf "personal namespace with duplicate name %s in group %s" $namespace $group.group) }}
    {{- end }}
    {{- $_ := set $uniqueDict $namespace "" }}
  {{- end }}
{{- end }}

{{- range $i, $group := .Values.sharedNamespaces }}
  {{- $namespace := $group.name }}
  {{- if hasKey $uniqueDict $namespace }}
    {{- fail (printf "shared namespace with duplicate name %s" $namespace) }}
  {{- end }}
  {{- $_ := set $uniqueDict $namespace "" }}
{{- end }}
