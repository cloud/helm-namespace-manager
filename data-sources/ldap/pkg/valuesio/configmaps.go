package valuesio

import (
	"context"
	"fmt"

	"gitlab.cern.ch/thartlan/ldap-data-source/pkg/types"

	"gopkg.in/yaml.v2"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

type ConfigMaps struct {
	Client       *kubernetes.Clientset
	InputCMName  string
	OutputCMName string
	Namespace    string
}

func NewConfigMaps(in, out, kubeconfig, namespace string) (*ConfigMaps, error) {
	var client *kubernetes.Clientset
	if kubeconfig == "" {
		config, err := rest.InClusterConfig()
		if err != nil {
			return nil, fmt.Errorf("could not get in cluster config: %v", err)
		}
		client, err = kubernetes.NewForConfig(config)
		if err != nil {
			return nil, fmt.Errorf("could not create kubernetes client: %v", err)
		}
	} else {
		config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
		if err != nil {
			return nil, fmt.Errorf("failed to build config from kubeconfig: %v", err)
		}
		client, err = kubernetes.NewForConfig(config)
		if err != nil {
			return nil, fmt.Errorf("could not create kubernetes client: %v", err)
		}
	}

	return &ConfigMaps{
		Client:       client,
		InputCMName:  in,
		OutputCMName: out,
		Namespace:    namespace,
	}, nil
}

func (c *ConfigMaps) GetValues() (*types.InputValues, error) {
	cm, err := c.Client.CoreV1().ConfigMaps(c.Namespace).Get(context.TODO(), c.InputCMName, metav1.GetOptions{})
	if err != nil {
		return nil, fmt.Errorf("could not read configmap: %v", err)
	}
	inputData := cm.Data["values.yaml"]
	var values types.InputValues
	err = yaml.Unmarshal([]byte(inputData), &values)
	if err != nil {
		return nil, fmt.Errorf("failed to parse values yaml: %v", err)
	}
	return &values, err
}

func (c *ConfigMaps) WriteValues(values *types.OutputValues) error {
	output, err := yaml.Marshal(&values)
	if err != nil {
		return fmt.Errorf("failed to marshal output yaml: %v", err)
	}
	data := map[string]string{"values.yaml": string(output)}

	cm, err := c.Client.CoreV1().ConfigMaps(c.Namespace).Get(context.TODO(), c.OutputCMName, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		cm := &corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name: c.OutputCMName,
			},
			Data: data,
		}
		_, err := c.Client.CoreV1().ConfigMaps(c.Namespace).Create(context.TODO(), cm, metav1.CreateOptions{})
		if err != nil {
			return fmt.Errorf("failed to create configmap: %v", err)
		}
	} else if err != nil {
		return fmt.Errorf("failed to get configmap for writing: %v", err)
	} else {
		cm.Data = data
		_, err := c.Client.CoreV1().ConfigMaps(c.Namespace).Update(context.TODO(), cm, metav1.UpdateOptions{})
		if err != nil {
			return fmt.Errorf("failed to update configmap: %v", err)
		}
	}
	return nil
}
