package types

type PersonalNamespace struct {
	Group string `yaml:"group"`
}

type InputValues struct {
	PersonalNamespaces []PersonalNamespace `yaml:"personalNamespaces"`
}

// Map of group names to list of members.
type GroupData map[string][]string

// Map of user names to map of attributes.
type UserData map[string]map[string]string

type OutputData struct {
	Groups GroupData `yaml:"groups"`
	Users  UserData  `yaml:"users"`
}

type OutputValues struct {
	Data OutputData `yaml:"data"`
}
