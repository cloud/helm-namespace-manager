module gitlab.cern.ch/thartlan/ldap-data-source

go 1.16

require (
        github.com/go-ldap/ldap v3.0.3+incompatible
        github.com/spf13/pflag v1.0.5
        gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
        gopkg.in/yaml.v2 v2.4.0
        k8s.io/api v0.22.1
        k8s.io/apimachinery v0.22.1
        k8s.io/client-go v0.22.1
)
